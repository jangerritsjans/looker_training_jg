view: rituals_yearofbirth {
  derived_table: {
    sql: select extract(year from dateofBirth), count(*) from `rituals-prod-1149.cdp_wip.profile_all_age`
      where extract(year from dateofBirth) between 1960 and 2000
      group by 1
      order by 1
       ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: f0_ {
    type: number
    sql: ${TABLE}.f0_ ;;
  }

  dimension: f1_ {
    type: number
    sql: ${TABLE}.f1_ ;;
  }

  set: detail {
    fields: [f0_, f1_]
  }
}
