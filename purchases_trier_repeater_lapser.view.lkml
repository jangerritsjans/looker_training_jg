view: purchases_trier_repeater_lapser {
  derived_table: {
    sql: WITH BASE AS
      (
      SELECT
      *
      FROM
      (
      SELECT contactId, countryId, currencyId, creationTime, orderValue_Euro
      , ROW_NUMBER() OVER(PARTITION BY contactid ORDER BY creationTime asc) AS rn_asc
      , ROW_NUMBER() OVER(PARTITION BY contactid ORDER BY creationTime desc) AS rn_desc
      FROM `rituals-prod-1149.cdp.purchase_all`
      WHERE contactId in
      (
      Select contactId FROM `rituals-prod-1149.cdp.purchase_all`
      --LIMIT 100
      )
      AND DATE(creationTime) between DATE_SUB(CURRENT_DATE(), INTERVAL 2 MONTH) AND CURRENT_DATE()
      order by contactId, creationTime
      )
      )
      select base.* , type.type_buyer
      from base
      left join
      (
      select
      contactid, CASE
        WHEN FIRST < DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH) AND LAST >= DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH) THEN 'REPEATER'
        WHEN LAST < DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH) THEN  'LAPSER'
        WHEN FIRST > DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH) THEN 'TRIER'
        ELSE 'UNKNOWN' END as TYPE_BUYER
      FROM
      (
      select contactid, date(min(creationTime)) as first, date(max(creationTime)) as last
      from base
      group by 1
      )
      ) type
      using (contactid)
       ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: contact_id {
    type: string
    sql: ${TABLE}.contactId ;;
  }

  dimension: country_id {
    type: string
    sql: ${TABLE}.countryId ;;
  }

  dimension: currency_id {
    type: string
    sql: ${TABLE}.currencyId ;;
  }

  dimension_group: creation_time {
    type: time
    sql: ${TABLE}.creationTime ;;
  }

  dimension: order_value_euro {
    type: number
    sql: ${TABLE}.orderValue_Euro ;;
  }

  dimension: rn_asc {
    type: number
    sql: ${TABLE}.rn_asc ;;
  }

  dimension: rn_desc {
    type: number
    sql: ${TABLE}.rn_desc ;;
  }

  dimension: type_buyer {
    type: string
    sql: ${TABLE}.type_buyer ;;
  }

  set: detail {
    fields: [
      contact_id,
      country_id,
      currency_id,
      creation_time_time,
      order_value_euro,
      rn_asc,
      rn_desc,
      type_buyer
    ]
  }
}
