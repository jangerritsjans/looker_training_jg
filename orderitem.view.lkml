view: orderitem {
  sql_table_name: cl_superstore.orderitem ;;

  dimension: discount {
    type: string
    sql: ${TABLE}.discount ;;
  }

  dimension: orderid {
    type: string
    # hidden: yes
    sql: ${TABLE}.orderid ;;
  }

  dimension: productid {
    type: string
    # hidden: yes
    sql: ${TABLE}.productid ;;
  }

  dimension: profit {
    type: number
    sql: ${TABLE}.profit ;;
  }

  dimension: quantity {
    type: number
    sql: ${TABLE}.quantity ;;
  }

  dimension: sales {
    type: number
    sql: ${TABLE}.sales ;;
  }

  measure: count {
    type: count
    drill_fields: [product.productname, product.productid, order.orderid]
  }
}
