view: order {
  sql_table_name: cl_superstore."order" ;;

  dimension: orderid {
    primary_key: yes
    type: string
    sql: ${TABLE}.orderid ;;
  }

  dimension: customerid {
    type: string
    sql: ${TABLE}.customerid ;;
  }

  dimension_group: orderdate {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.orderdate ;;
  }

  dimension: ordervalue {
    type: number
    sql: ${TABLE}.ordervalue ;;
  }

  dimension_group: shipdate {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.shipdate ;;
  }

  dimension: shipmode {
    type: string
    sql: ${TABLE}.shipmode ;;
  }

  measure: count {
    type: count
    drill_fields: [orderid]
  }
}
