view: rituals_yearbirth {
  derived_table: {
    sql: select countryCode, extract(year from dateofBirth) as year, count(*) as number_of_contactids from `rituals-prod-1149.cdp_wip.profile_all_age`
      where extract(year from dateofBirth) between 1960 and 2000
      group by 1, 2
      order by 1
       ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: year {
    type: number
    sql: ${TABLE}.year ;;
  }

  dimension: number_of_contactids {
    type: number
    sql: ${TABLE}.number_of_contactids ;;
  }

  set: detail {
    fields: [year, number_of_contactids]
  }
}
