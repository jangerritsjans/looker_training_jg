view: rituals_purchase_fixed_dates {
  derived_table: {
    sql: SELECT
      CASE WHEN rn_asc =1 and date(creationTime) < '2020-08-01' THEN 'FIRST'
          ELSE 'SECOND' END as start,
      *
      FROM
      (
      SELECT contactId, countryId, currencyId, creationTime, orderValue_Euro
      , ROW_NUMBER() OVER(PARTITION BY contactid ORDER BY creationTime asc) AS rn_asc
      , ROW_NUMBER() OVER(PARTITION BY contactid ORDER BY creationTime desc) AS rn_desc
      FROM `rituals-prod-1149.cdp.purchase_all`
      WHERE contactId in
      (
      Select contactId FROM `rituals-prod-1149.cdp.purchase_all`
      --LIMIT 100
      )
      AND DATE(creationTime) between '2020-07-01' AND '2020-09-01'
      order by contactId, creationTime
      )
      ;
       ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: start {
    type: string
    sql: ${TABLE}.start ;;
  }

  dimension: contact_id {
    type: string
    sql: ${TABLE}.contactId ;;
  }

  dimension: country_id {
    type: string
    sql: ${TABLE}.countryId ;;
  }

  dimension: currency_id {
    type: string
    sql: ${TABLE}.currencyId ;;
  }

  dimension_group: creation_time {
    type: time
    sql: ${TABLE}.creationTime ;;
  }

  dimension: order_value_euro {
    type: number
    sql: ${TABLE}.orderValue_Euro ;;
  }

  dimension: rn_asc {
    type: number
    sql: ${TABLE}.rn_asc ;;
  }

  dimension: rn_desc {
    type: number
    sql: ${TABLE}.rn_desc ;;
  }

  set: detail {
    fields: [
      start,
      contact_id,
      country_id,
      currency_id,
      creation_time_time,
      order_value_euro,
      rn_asc,
      rn_desc
    ]
  }
}
