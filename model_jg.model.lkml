connection: "crystalloids-si_redshift_dev"

include: "*.view"

datagroup: cl_jg_superstore_default_datagroup {
  max_cache_age: "1 hour"
}

persist_with: cl_jg_superstore_default_datagroup

explore: orderitem {
join: order {
  type: left_outer
  relationship: many_to_one
  sql_on: ${orderitem.orderid} = ${order.orderid} ;;
}
}
explore: order {}

explore: customer {}
