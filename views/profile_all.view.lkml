view: profile_all {
  sql_table_name: `rituals-dev.cdp.profile_all`
    ;;

  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: contact_id {
    type: string
    sql: ${TABLE}.contactId ;;
  }

  dimension: country_code {
    type: string
    sql: ${TABLE}.countryCode ;;
  }

  dimension_group: creation {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.creationDate ;;
  }

  dimension: employee {
    type: yesno
    sql: ${TABLE}.employee ;;
  }

  dimension: favourite_shop {
    type: string
    sql: ${TABLE}.favouriteShop ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension: loyalty {
    hidden: yes
    sql: ${TABLE}.loyalty ;;
  }

  dimension: marketing_cookie {
    type: string
    sql: ${TABLE}.marketingCookie ;;
  }

  dimension_group: marketing_cookie {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.marketingCookieDate ;;
  }

  dimension_group: optin {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.optinDate ;;
  }

  dimension: optin_newsletter_general {
    type: yesno
    sql: ${TABLE}.optinNewsletterGeneral ;;
  }

  dimension: optin_newsletter_general_confirmed {
    type: yesno
    sql: ${TABLE}.optinNewsletterGeneralConfirmed ;;
  }

  dimension: optin_newsletter_general_subscribe_sorce {
    type: string
    sql: ${TABLE}.optinNewsletterGeneralSubscribeSorce ;;
  }

  dimension: optin_newsletter_loyalty {
    type: yesno
    sql: ${TABLE}.optinNewsletterLoyalty ;;
  }

  dimension: optin_sms {
    type: yesno
    sql: ${TABLE}.optinSMS ;;
  }

  dimension_group: optout {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.optoutDate ;;
  }

  dimension: preferred_locale {
    type: string
    sql: ${TABLE}.preferredLocale ;;
  }

  dimension: subscribe_source_deep {
    type: string
    sql: ${TABLE}.subscribeSourceDeep ;;
  }

  dimension: subscribe_store_number {
    type: string
    sql: ${TABLE}.subscribeStoreNumber ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}

view: profile_all__loyalty {
  dimension_group: issue {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.issueDate ;;
  }

  dimension: loyalty_id {
    type: string
    sql: ${TABLE}.loyaltyId ;;
  }

  dimension: loyalty_type {
    type: string
    sql: ${TABLE}.loyaltyType ;;
  }

  dimension: reserved_for {
    type: string
    sql: ${TABLE}.reservedFor ;;
  }

  dimension: store_number {
    type: string
    sql: ${TABLE}.storeNumber ;;
  }

  dimension: subscribe_source {
    type: string
    sql: ${TABLE}.subscribeSource ;;
  }

  dimension: tier {
    type: string
    sql: ${TABLE}.tier ;;
  }

  dimension: welcome_gift_redeemed {
    type: yesno
    sql: ${TABLE}.welcomeGiftRedeemed ;;
  }
}
