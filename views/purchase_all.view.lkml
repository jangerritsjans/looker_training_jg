view: purchase_all {
  sql_table_name: `rituals-dev.cdp.purchase_all`
    ;;

  dimension: contact_id {
    type: string
    sql: ${TABLE}.contactId ;;
  }

  dimension: country_id {
    type: string
    sql: ${TABLE}.countryId ;;
  }

  dimension: order_value_euro {
    type: number
    sql: ${TABLE}.orderValue_Euro ;;
  }

  dimension: order_value_local {
    type: number
    sql: ${TABLE}.orderValue_Local ;;
  }

  dimension_group: payment {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.paymentTime ;;
  }

  dimension_group: shipment {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.shipmentDate ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
