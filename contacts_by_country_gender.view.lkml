view: contacts_by_country_gender {
  derived_table: {
    sql: select gender, countryCode, contactid FROM `rituals-dev.cdp.profile_all` where extract(month from optinDate) >=10 and  extract(year from optinDate) = 2019
      ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension: country_code {
    type: string
    sql: ${TABLE}.countryCode ;;
  }

  dimension: contactid {
    type: string
    sql: ${TABLE}.contactid ;;
  }

  set: detail {
    fields: [gender, country_code, contactid]
  }
}
