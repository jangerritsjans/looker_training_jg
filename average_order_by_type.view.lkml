view: average_order_by_type {
  derived_table: {
    sql: WITH BASE AS
      (
      SELECT
      *
      FROM
      (
      SELECT contactId, countryId, creationTime, orderValue_Euro
      FROM `rituals-prod-1149.cdp.purchase_all`
      WHERE contactId in
      (
      Select contactId FROM `rituals-prod-1149.cdp.purchase_all`
      --LIMIT 100
      )
      AND DATE(creationTime) between date_trunc(DATE_SUB(CURRENT_DATE(), INTERVAL 2 MONTH),MONTH) AND date_trunc(CURRENT_DATE(),MONTH)
      order by contactId, creationTime
      )
      )
      select base.* , type.type_buyer
      from base
      left join
      (
      select
      contactid, CASE
        WHEN FIRST < date_trunc(DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH),MONTH) AND LAST >= DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH) THEN 'REPEATER'
        WHEN LAST < date_trunc(DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH),MONTH) THEN  'LAPSER'
        WHEN FIRST >= date_trunc(DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH),MONTH) THEN 'TRIER'
        ELSE 'UNKNOWN' END as TYPE_BUYER
      FROM
      (
      select contactid, date(min(creationTime)) as first, date(max(creationTime)) as last
      from base
      group by 1
      )
      ) type
      using (contactid)
       ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: contact_id {
    type: string
    sql: ${TABLE}.contactId ;;
  }

  dimension: country_id {
    type: string
    sql: ${TABLE}.countryId ;;
  }

  dimension_group: creation_time {
    type: time
    sql: ${TABLE}.creationTime ;;
  }

  dimension: order_value_euro {
    type: number
    sql: ${TABLE}.orderValue_Euro ;;
  }

  dimension: type_buyer {
    type: string
    sql: ${TABLE}.type_buyer ;;
  }

  set: detail {
    fields: [contact_id, country_id, creation_time_time, order_value_euro, type_buyer]
  }
}
